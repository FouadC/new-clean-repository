/**
 * 
 */

/**
 * @author Fouad
 *
 */
public class MicroTest {

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Microtest {
 
	
	@Test 
	public void Should_Return_Micro_Name() {
		
		Micro micro = new Micro();
		
		String name = micro.name("dell");
		
		Assertions.assertEquals("dell", name);
		
		
	}

}
}
